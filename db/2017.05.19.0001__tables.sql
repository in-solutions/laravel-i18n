CREATE TABLE `enm_country` (
  `id` INT(10) UNSIGNED NOT NULL,
  `iso_code` VARCHAR(2) NOT NULL COMMENT 'ISO 3166-1 alpha-2',
  `iso3_code` VARCHAR(3) NOT NULL COMMENT 'ISO 3166-1 alpha-3 ',
  `name` VARCHAR(50) NOT NULL,
  `continent` VARCHAR(2) NOT NULL,
  `in_eu` TINYINT(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `iso_code` (`iso_code`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

CREATE TABLE `enm_currency` (
  `id` INT(10) UNSIGNED NOT NULL,
  `code` VARCHAR(5) NOT NULL COMMENT 'ISO 4217',
  `symbol` VARCHAR(50) NOT NULL,
  `symbol_position` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code` (`code`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;

CREATE TABLE `enm_language` (
  `id` INT(11) UNSIGNED NOT NULL,
  `iso_code` VARCHAR(2) NOT NULL COMMENT 'ISO 639-1',
  `name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `iso_code` (`iso_code`)
)
  COLLATE='utf8_general_ci'
  ENGINE=InnoDB;