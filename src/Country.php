<?php

namespace Insolutions\I18n;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Insolutions\I18n\Country
 *
 * @property int $id
 * @property string $iso_code ISO 3166-1 alpha-2
 * @property string $iso3_code ISO 3166-1 alpha-3
 * @property string $name
 * @property string $capital
 * @property string $capitalZip
 * @property string $continent
 * @property int $in_eu
 * @property-read \Illuminate\Database\Eloquent\Collection|\Insolutions\I18n\Domain[] $domains
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\I18n\Country whereContinent($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\I18n\Country whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\I18n\Country whereInEu($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\I18n\Country whereIso3Code($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\I18n\Country whereIsoCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\I18n\Country whereName($value)
 * @mixin \Eloquent
 */
class Country extends Model
{

    protected $table = 'enm_country';
}