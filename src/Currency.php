<?php

namespace Insolutions\I18n;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Insolutions\I18n\Currency
 *
 * @property int $id
 * @property string $iso_code ISO 4217
 * @property string $name
 * @property string $sign
 * @property-read \Illuminate\Database\Eloquent\Collection|\Insolutions\I18n\Domain[] $domains
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\I18n\Currency whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\I18n\Currency whereIsoCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\I18n\Currency whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\I18n\Currency whereSign($value)
 * @mixin \Eloquent
 */
class Currency extends Model
{

    protected $table = 'enm_currency';

}