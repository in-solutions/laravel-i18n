<?php

namespace Insolutions\I18n\Facades;

use Insolutions\I18n\Exceptions\CurrencyNotSetException;
use Insolutions\I18n\Models\Currency;

class Curr
{
    private static $currency = null;

    public static function set(Currency $currency)
    {
        self::$currency = $currency;
    }

    public static function get(): Currency
    {
        if (self::$currency === null) {
            throw new CurrencyNotSetException('You must call Curr::set(Currency) before trying to get it.');
        }
        return self::$currency;
    }
}