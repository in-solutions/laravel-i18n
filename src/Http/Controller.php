<?php

namespace Insolutions\I18n\Http;

use Insolutions\I18n\Country;
use Insolutions\I18n\Currency;
use Insolutions\I18n\Language;

class Controller extends \App\Http\Controllers\Controller
{

    public function getCountries()
    {
        $countries = Country::orderBy('name')->get();
        return response()->json($countries);
    }

    public function getCurrencies()
    {
        return response()->json(Currency::all());
    }

    public function getLanguages()
    {
        $languages = Language::orderBy('name')->get();
        return response()->json($languages);
    }
}