<?php

namespace Insolutions\I18n\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;
use Insolutions\I18n\Facades\Curr;
use Insolutions\I18n\Currency;

class CurrencyResolver
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            /* @var $currency Currency */
            $currency = Auth::user()->client->currency;
            Curr::set($currency);
        } else {
        	Curr::set(Currency::first()); // default currency
        }

        return $next($request);
    }
}
