<?php

namespace Insolutions\I18n\Http\Middleware;

use App;
use Auth;
use Closure;
use Illuminate\Http\Request;

class LanguageResolver
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        App::setLocale(self::getLanguageCode());
        return $next($request);
    }

    public static function getLanguageCode(): string
    {
        $languageCode = 'en';
        if (Auth::check()) {
            $languageCode = Auth::getUser()->client->language_code;
        }
        return $languageCode;
    }
}
