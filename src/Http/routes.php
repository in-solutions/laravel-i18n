<?php

use Insolutions\I18n\Http\Controller;

Route::group(['middleware' => ['web']], function () {
    Route::get('country', Controller::class . '@getCountries');
    Route::get('language', Controller::class . '@getLanguages');
    Route::get('currency', Controller::class . '@getCurrencies');

});