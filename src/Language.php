<?php

namespace Insolutions\I18n;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Insolutions\I18n\Language
 *
 * @property int $id
 * @property string $iso_code ISO 639-1
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\Insolutions\I18n\Domain[] $domains
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\I18n\Language whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\I18n\Language whereIsoCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Insolutions\I18n\Language whereName($value)
 * @mixin \Eloquent
 */
class Language extends Model
{

    const FK_NAME = 'language_code';

    protected $table = 'enm_language';

}