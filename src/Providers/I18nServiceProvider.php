<?php

namespace Insolutions\I18n\Providers;

use Illuminate\Support\ServiceProvider;
use Insolutions\I18n\Http\Controller;

class I18nServiceProvider extends ServiceProvider
{

    public function boot()
    {        
        
        $this->publishes([
            __DIR__.'/../../db' => base_path('database/sql/insolutions/i18n'),
//            __DIR__.'/../app/Listeners' => app_path('Listeners'),
//            __DIR__.'/views' => base_path('resources/views/insolutions/i18'),        
//            __DIR__.'/assets' => public_path('ins/ecommerce'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // register routes
        include __DIR__ . '/../Http/routes.php';

        // register controllers
        $this->app->make(Controller::class);
    }
}
